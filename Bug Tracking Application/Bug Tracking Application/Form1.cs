﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Bug_Tracking_Application
{
    public partial class LoginForm : Form
    {
        DatabaseConnection objConnect;//to store connection object
        string conString;//for connection string
        DataTable ds;//to load all the database data into this dataset
        public string tString;
        public int Password;
        public const String validation = "Please Enter Your ID and Password";
 

        public LoginForm()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)//Exit the application when Exit on the login form is clicked
        {
            Application.Exit();//exit method of the application
        }

        private void btnLogin_Click(object sender, EventArgs e)//when login button is clicked
        {

            if (txtId.Text == "" || maskedTextBox1.Text == "")//if textboxes are empty
            {
                MessageBox.Show(validation);//show this
            }

            tString = txtId.Text;//local variable stores value from the textbox

            if (tString.Trim() == "") return;//if id box is empty loops and checks the first character if the input is not an integer shows the message shown below


            for (int i = 0; i < tString.Length; i++)
            {
                if (!char.IsNumber(tString[i]))//checks the first character
                {
                    MessageBox.Show("Please enter a valid id");//show this
                    txtId.Text = "";//empty the input box
                    return;
                }

            }

            objConnect = new DatabaseConnection();//new object from the class
            conString = Properties.Settings.Default.bugAppConnectionString;//access the connection string from the properties
            objConnect.connection_string = conString;//This allows us to pass over the connection string to our DatabaseConnection class.
            objConnect.Sql = "SELECT COUNT(*) FROM employees WHERE empId='" + txtId.Text + "' AND password='" + maskedTextBox1.Text + "'";//query checking the employees table
            ds = objConnect.GetConnection;//call GetConnection method from the DatabaseConnection class and hand its DataSet over to the variable ds

        }


        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }




}




