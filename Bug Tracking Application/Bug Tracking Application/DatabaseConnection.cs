﻿using System.Data;
using System.Windows.Forms;

namespace Bug_Tracking_Application
{
   public class DatabaseConnection
    {
        private string sql_string;//will hold a SQL string.
        private string strCon; //hold a location of the database
        System.Data.SqlClient.SqlDataAdapter da_1; //A DataAdapter is used to open up a table in a database


        public string Sql //write-only property for sql query
        {
            set { sql_string = value; }//set the variable called sql_string to whatever is held in the Sql variable
        }
        public string connection_string //write-nly property for connection_string
        {
            set { strCon = value; }//set the variable called strCon to whatever is held in the connection_string variable
        }

        public System.Data.DataTable GetConnection//DataTable holds the data from datatbase
        {
            get { return MyDataSet(); }//The method connects to the database and fills the Dataset - in our case data table
        }

        private System.Data.DataTable MyDataSet()//load table data into a DataSet, rather than manipulate the table in the database.

        {
            //sqlConnection will use the connection string in strCon to connect to the database.
            System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(strCon);
            con.Open();//opens the connection
            da_1 = new System.Data.SqlClient.SqlDataAdapter(sql_string, con);//DataAdapter used to open up a table in a db, sql_string is the query and con is the connection
            DataTable dt = new DataTable(); //this is creating a virtual table  
            da_1.Fill(dt);//fill method of the sqldatadpater used to fill the datatable


            if (dt.Rows[0][0].ToString() == "1")//if record exists 
            {

                LoginForm loginf = new LoginForm(); //new object from the loginform
                loginf.Hide();//hides the login form
                new Bugs().Show();//shows the Bugs form
            }
            else//else 
            {
                MessageBox.Show("Invalid id or password");//show this

            }

            con.Close();//closes the connection
            return dt;//returns the datatable
        }



    }
}
