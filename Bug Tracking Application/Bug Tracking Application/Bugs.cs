﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Bug_Tracking_Application
{
    public partial class Bugs : Form
    {
        SqlCommand sCommand;//executes sql commands on a database.
        SqlDataAdapter sAdapter; //interacts with the DataTable type.It can fill a DataTable with a table SQL Server database.
        SqlDataAdapter adapt;
        SqlCommandBuilder sBuilder;//helps UPDATE SQL Server tables
        DataSet sDs;//collection of DataTables. 
        DataTable sTable;//stores rows and columns of data
        DataTable dt;
        private int rowIndex = 0;
        int scrollVal;
        

        public Bugs()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.RowTemplate;//access the datagrideview rows
            row.DefaultCellStyle.BackColor = Color.Bisque;//row color
            row.Height = 35;// row height
            row.MinimumHeight = 20;//row min height

            string connectionString = Properties.Settings.Default.bugAppConnectionString;//connection string
            string sql = "SELECT * FROM bugs";//query selecting all from bugs table
            SqlConnection connection = new SqlConnection(connectionString);//create a connection and passing in the connection string
            connection.Open();//opens the connection
            sCommand = new SqlCommand(sql, connection);//new object from sqlCommand
            sAdapter = new SqlDataAdapter(sCommand);//new object from sqlDataAdapter
            sBuilder = new SqlCommandBuilder(sAdapter);//new object from sqlCommandBuilder
            sDs = new DataSet();//new DataSet object
            sAdapter.Fill(sDs, scrollVal, 5, "bugs");//fills the adapter with the bugs table records
            sTable = sDs.Tables["bugs"];//gets the collection of tables contained in the Dataset
            connection.Close();//closes the connection

            //populating the data into the datagrid
            dataGridView1.DataSource = sDs.Tables["bugs"];
            dataGridView1.DataSource = sDs;
            dataGridView1.DataMember = "bugs";
            dataGridView1.ReadOnly = true;

            btnSave.Enabled = false;//sets to false
            
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect; //row selection mode

            dataGridView1.RowsDefaultCellStyle.BackColor = Color.Bisque;//cell style
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;//cell backcolor
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.None;//cell border style

            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.Red;//selection backcolor
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Yellow;// selection forecolor

            dataGridView1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;//wrapping 
            dataGridView1.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;//alignment

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect; //selection mode set to fullrowselect
            dataGridView1.AllowUserToResizeColumns = false;//sets resizing to false
        }

        private void btnAddEdit_Click(object sender, EventArgs e)//when add clicked
        {
            dataGridView1.ReadOnly = false;//set datagrid view to false
            btnSave.Enabled = true;// set save button to true
            btnAddEdit.Enabled = false; //set AddEdit to false
            btnDelete.Enabled = false; //set Delete to false
        }

        private void btnSave_Click(object sender, EventArgs e)//when save clicked
        {
            sAdapter.Update(sTable);//updates the data table
            dataGridView1.ReadOnly = true; //sets the datagrid to readonly
            btnSave.Enabled = false; // set save to false
            btnAddEdit.Enabled = true; // set AddEdit to true
            btnDelete.Enabled = true; //set Delete to true
            MessageBox.Show("Record Saved");//show message
        }

        private void btnDelete_Click(object sender, EventArgs e)//when delete clicked
        {
            if (MessageBox.Show("Do you want to delete this row ?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)//show this - if Yes...
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);//remove the record
                sAdapter.Update(sTable);//update the data table
            }
        }

        private void btnNext_Click(object sender, EventArgs e)//when next clicked
        {
            scrollVal = scrollVal + 5; //scrolls by five records

            if (scrollVal > 23)//if statement checking if the scrollVal is greater than 23
            {
                scrollVal = 18; //sets this to 18
            }

            sDs.Clear();//clear the data table
            sAdapter.Fill(sDs, scrollVal, 5, "bugs");//fills the adapter by the records
        }


        private void btnPrev_Click(object sender, EventArgs e)//when previous clicked
        {
            scrollVal = scrollVal - 5; //scrolls backward by five
            if (scrollVal <= 0) //if less than or equals zero
            {
                scrollVal = 0; //sets to zero
            }
            sDs.Clear();//clear the data table
            sAdapter.Fill(sDs, scrollVal, 5, "bugs");// fills the adapter by the records
        }


        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)//if right click performed on a cell
            {
                //selects the particular row and show the contextmenustrip - delete button
                this.dataGridView1.Rows[e.RowIndex].Selected = true;
                this.rowIndex = e.RowIndex;
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[e.RowIndex].Cells[1];
                this.contextMenuStrip1.Show(this.dataGridView1, e.Location);
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void contextMenuStrip1_Click(object sender, EventArgs e)
        {
            //if Yes choosen removes the record and updates the adapter
            if (MessageBox.Show("Do you want to delete this row ?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
                sAdapter.Update(sTable);
            }
        }

        private void btnSort_Click(object sender, EventArgs e)
        {
            dataGridView1.Sort(dataGridView1.Columns[7], ListSortDirection.Ascending);//sorts the records
        }

        private void textBox1_TextChanged(object sender, EventArgs e)//searchbox
        {
            string con = Properties.Settings.Default.bugAppConnectionString;//storing the connection string
            SqlConnection connection = new SqlConnection(con);//new connection
            connection.Open(); //opens the connection
           
            adapt = new SqlDataAdapter("select * from bugs where empId like '" + textBox1.Text + "%'", connection);//new adapter takes the sql and connection as parameters
            dt = new DataTable();//new data table
            adapt.Fill(dt);//fills the adapter
            dataGridView1.DataSource = dt;//data source for the datagrideview
            connection.Close();//closes the connection
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
